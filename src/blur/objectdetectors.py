import math
import cv2
import numpy as np
import torch
import logging
from yolov6.utils.events import load_yaml
from yolov6.layers.common import DetectBackend
from yolov6.utils.nms import non_max_suppression
from yolov6.data.data_augment import letterbox
from . import precess
import yolov6.utils.events

YOLO_RELEASE="0.1.0"
yolov6.utils.events.LOGGER.setLevel(logging.WARNING) # silence some info from YOLO

class Inferer(object):
    """An object detection model"""

    def infer(self, image):
        """Finds the bounding boxes of objects in the given image.

        Parameters
        ----------
        image : PIL.Image
            The image to search in
        """
        raise Exception("Unimplemented")


class FastInferer(Inferer):
    ###
    ### Based on yolov6/code/inferer
    ###

    WEIGHTS_NAME = 'yolov6s_weights.pt'
    LABELS_NAME = 'yolov6s_labels.yaml'
    WEIGHTS_DOWNLOAD_URL = 'https://github.com/meituan/YOLOv6/releases/download/'+YOLO_RELEASE+'/yolov6s.pt'
    LABELS_DOWNLOAD_URL = 'https://github.com/meituan/YOLOv6/raw/'+YOLO_RELEASE+'/data/coco.yaml'

    def __init__(self, image_size, confidanceThreshold, iouThreshold):
        self.__dict__.update(locals())
        useGPUIfPossible = False                                    # TODO Test whether blur models can be run on gpu (see blur/readme)
        cuda = useGPUIfPossible and torch.cuda.is_available()
        self.device = torch.device('cuda:0' if cuda else 'cpu')
        self.conf_threshold = confidanceThreshold
        self.iou_threshold = iouThreshold
        self.image_size = image_size
        self.device = torch.device(self.device)
        self.model = DetectBackend(precess.getModelFile(self.WEIGHTS_NAME), device=self.device)
        self.stride = self.model.stride
        self.class_names = load_yaml(precess.getModelFile(self.LABELS_NAME))['names']
        self.image_size = self.check_image_size(self.image_size, s=self.stride)
        self.model.model.float()
        self.detected_classes = [self.class_names.index(clazz) for clazz in ('car', 'person', 'motorcycle', 'truck', 'bus')]
        if self.device.type != 'cpu':
            self.model(torch.zeros(1, 3, *self.img_size).to(self.device).type_as(next(self.model.model.parameters())))


    def infer(self, image):
        ''' Model Inference and results visualization '''
        image = cv2.cvtColor(np.array(image), cv2.COLOR_RGB2BGR)
        precessed_image = self.precess_image(image, self.image_size, self.stride)
        precessed_image = precessed_image.to(self.device)
        if len(precessed_image.shape) == 3:
            precessed_image = precessed_image[None]
        prediction_results = self.model(precessed_image)
        det = non_max_suppression(prediction_results, self.conf_threshold, self.iou_threshold, self.detected_classes, max_det=1000)[0]

        gn = torch.tensor(image.shape)[[1, 0, 1, 0]]

        boxes = []

        if len(det):
            det[:, :4] = self.rescale(precessed_image.shape[2:], det[:, :4], image.shape).round()

            for *xyxy, confidence, cls in reversed(det):
                xywh = (self.box_convert(torch.tensor(xyxy).view(1, 4)) / gn).view(-1).tolist()
                boxes.append((cls, *xywh, confidence))

        return boxes


    @staticmethod
    def precess_image(image, image_size, stride):
        image = letterbox(image, image_size, stride=stride)[0]

        image = image.transpose((2, 0, 1))[::-1]
        image = torch.from_numpy(np.ascontiguousarray(image))
        image = image.float()
        image /= 255

        return image


    @staticmethod
    def rescale(original_shape, boxes, target_shape):
        '''Rescale the output to the original image shape'''
        ratio = min(original_shape[0] / target_shape[0], original_shape[1] / target_shape[1])
        padding = (original_shape[1] - target_shape[1] * ratio) / 2, (original_shape[0] - target_shape[0] * ratio) / 2

        boxes[:, [0, 2]] -= padding[0]
        boxes[:, [1, 3]] -= padding[1]
        boxes[:, :4] /= ratio

        boxes[:, 0].clamp_(0, target_shape[1])
        boxes[:, 1].clamp_(0, target_shape[0])
        boxes[:, 2].clamp_(0, target_shape[1])
        boxes[:, 3].clamp_(0, target_shape[0])

        return boxes

    def check_image_size(self, img_size, s=32, floor=0):
        """Make sure image size is a multiple of stride s in each dimension, and return a new shape list of image."""
        if isinstance(img_size, int):
            new_size = max(self.make_divisible(img_size, int(s)), floor)
        elif isinstance(img_size, list):
            new_size = [max(self.make_divisible(x, int(s)), floor) for x in img_size]
        else:
            raise Exception(f"Unsupported type of img_size: {type(img_size)}")

        if new_size != img_size:
            print(f'WARNING: --img-size {img_size} must be multiple of max stride {s}, updating to {new_size}')
        return new_size if isinstance(img_size, list) else [new_size]*2


    def make_divisible(self, x, divisor):
        return math.ceil(x / divisor) * divisor


    @staticmethod
    def box_convert(x):
        y = x.clone() if isinstance(x, torch.Tensor) else np.copy(x)
        y[:, 0] = (x[:, 0] + x[:, 2]) / 2
        y[:, 1] = (x[:, 1] + x[:, 3]) / 2
        y[:, 2] = x[:, 2] - x[:, 0]
        y[:, 3] = x[:, 3] - x[:, 1]
        return y
