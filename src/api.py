from fastapi import FastAPI, UploadFile, Depends, Response
from PIL import Image
import io
import gc
from .blur import blur
from pydantic import BaseSettings

app = FastAPI()
print("API is preparing to start...")


class Settings(BaseSettings):
	strategy: str = "COMPROMISE"
	webp_method: int = 6

settings = Settings()

blur.blurPreinit({
	"BLUR_STRATEGY": settings.strategy,
	"MODELS_FS_URL": "./models",
})


@app.get("/")
async def root():
	return {"message": "GeoVisio Blurring API"}


@app.post(
	"/mask/",
	responses = {200: {"content": {"image/png": {}}}},
	response_class=Response
)
async def blur_mask(picture: UploadFile):
	sentPic = Image.open(picture.file)
	blurMask = blur.getBlurMask(sentPic)

	blurMaskIo = io.BytesIO()
	blurMask.save(blurMaskIo, format="png", optimize=True, bits=1)
	blurMaskIo.seek(0)

	# For some reason garbage collection does not run automatically after
	# a call to an AI model, so it must be done explicitely
	gc.collect()

	return Response(content=blurMaskIo.getvalue(), media_type="image/png")


@app.post(
	"/blur/",
	responses = {200: {"content": {"image/webp": {}}}},
	response_class=Response
)
async def blur_picture(picture: UploadFile):
	sentPic = Image.open(picture.file)
	blurMask = blur.getBlurMask(sentPic)

	blurredPic = blur.blurPicture(sentPic, blurMask)
	blurredPicIo = io.BytesIO()
	blurredPic.save(blurredPicIo, format="webp", method=int(settings.webp_method), quality=80)
	blurredPicIo.seek(0)

	# For some reason garbage collection does not run automatically after
	# a call to an AI model, so it must be done explicitely
	gc.collect()

	return Response(content=blurredPicIo.getvalue(), media_type="image/webp")
