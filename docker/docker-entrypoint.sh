#!/bin/bash

usage() {
    echo "./docker-entrypoint.sh <COMMAND>: "
    echo -e "\tThis script simplifies running GeoVisio blurring in a certain mode"
    echo "Commands: "
    echo -e "\tapi: Starts web API for production on port 8000 by default"
    echo -e "\tcli: Process pictures using command-line script"
}

command=${1}
echo "Executing \"${command}\" command"

case $command in
"api")
    uvicorn src.api:app --host 0.0.0.0 --port 80
    ;;
"cli")
    python ./src/main.py
    ;;
*)
    usage
    ;;
esac
