import pytest
import os
from dotenv import load_dotenv

# set env var
os.environ["STRATEGY"] = "FAST"
os.environ["WEBP_METHOD"] = "0"

FIXTURE_DIR = os.path.join(
	os.path.dirname(os.path.realpath(__file__)),
	'data'
)

IMG1 = pytest.mark.datafiles(os.path.join(FIXTURE_DIR, '1.jpg'))
MASK1 = pytest.mark.datafiles(os.path.join(FIXTURE_DIR, '1_mask.png'))
BLUR1 = pytest.mark.datafiles(os.path.join(FIXTURE_DIR, '1_blurred.webp'))
